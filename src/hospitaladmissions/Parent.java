/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospitaladmissions;

/**
 * Class Parent extends Person
 * @author Adeel Khilji
 */
public class Parent extends Person
{
    private String relationship;//Declaring instance variable
    
    /**
     * Constructor with two parameters
     * @param name String
     * @param relationship String
     */
    protected Parent(String name, String relationship)
    {
        super(name);
        this.relationship = relationship;
    }
    
    /**
     * getRelationship - getter method
     * @return String
     */
    public String getRelationship()
    {
        return this.relationship;
    }
    
    /**
     * toString - overridden method
     * @return String
     */
    public String toString()
    {
        return "PARENT NAME: " + super.getName() + " RELATIONSHIP: " + getRelationship();
    }
}
