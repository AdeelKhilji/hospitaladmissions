/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospitaladmissions;

import java.util.List;

/**
 * Class Patient extends Person
 * @author Adeel Khilji
 */
public class Patient extends Person
{
    private List<WristBand> wristBands;//Declaring instance variable
    private List<AllergicWristBand> allergicWristBands;//Declaring instance variable
    private WristBandForChild wristBandForChild;//Declaring instance variable
    private ResearchGroup researchGroup;//Declaring instance variable
    
    /**
     * Constructor with five parameters
     * @param name String
     * @param wristBands List<WristBand>
     * @param allergicWristBands List<AllergicWristBand>
     * @param wristBandForChild List<WristBandForChild>
     * @param researchGroup ResearchGroup
     */
    protected Patient(String name, List<WristBand> wristBands, List<AllergicWristBand> allergicWristBands, WristBandForChild wristBandForChild, ResearchGroup researchGroup)
    {
        super(name);
        this.wristBands = wristBands;
        this.allergicWristBands = allergicWristBands;
        this.wristBandForChild = wristBandForChild;
        this.researchGroup = researchGroup;
    }
    /**
     * getWristBands - getter method
     * @return List<WristBand>
     */
    public List<WristBand> getWristBands()
    {
        return this.wristBands;
    }
    /**
     * getAllergicWristBands - getter method
     * @return List<AllergicWristBand>
     */
    public List<AllergicWristBand> getAllergicWristBands()
    {
        return this.allergicWristBands;
    }
    /**
     * getWristBandForChild - getter method
     * @return WristBandForChild
     */
    public WristBandForChild getWristBandForChild()
    {
        return this.wristBandForChild;
    }
    /**
     * getResearchGroup - getter method
     * @return ResearchGroup
     */
    public ResearchGroup getResearchGroup()
    {
        return this.researchGroup;
    }
    /**
     * toString - overridden method
     * @return String
     */
    @Override
    public String toString()
    {
        return super.getName() + "\nWRIST BANDS: " + getWristBands() + "\nALLERGY WRISTBANDS: " + getAllergicWristBands() + "\nWRISTBAND FOR CHILD: " + getWristBandForChild() + "\nRESEARCH GROUP: " + getResearchGroup();
    }
}
