/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospitaladmissions;

/**
 * Class BarCode
 * @author Adeel Khilji
 */
public class BarCode 
{
    private String barCode;//Declaring instance variable
    
    /**
     * Constructor with one parameter
     * @param barCode String
     */
    protected BarCode(String barCode)
    {
        this.barCode = barCode;
    }
    
    /**
     * getBarCode - getter method
     * @return String
     */
    public String getBarCode()
    {
        return this.barCode;
    }
    
    /**
     * toString - overridden method
     * @return String
     */
    @Override
    public String toString()
    {
        return getBarCode();
    }
}
