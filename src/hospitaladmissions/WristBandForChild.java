/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospitaladmissions;

import java.util.List;

/**
 * Class WristBandForChild extends WristBand
 * @author Adeel Khilji
 */
public class WristBandForChild extends WristBand
{
    private List<Parent> parents;//Declaring instance variable
    
    /**
     * Constructor with three parameters
     * @param barCode BarCode
     * @param medicalDetails MedicalDetails
     * @param parents List<Parent>
     */
    protected WristBandForChild(BarCode barCode, MedicalDetails medicalDetails, List<Parent> parents)
    {
        super(barCode, medicalDetails);
        this.parents = parents;
    }
    /**
     * getParents - getter method
     * @return List<Parent>
     */
    public List<Parent> getParents()
    {
        return this.parents;
    }
    /**
     * toString - overridden method
     * @return String
     */
    @Override
    public String toString()
    {
        return super.toString() + "\nPARENTS: " + getParents();
    }
}
