/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospitaladmissions;

import java.util.List;

/**
 * Class ResearchGroup
 * @author Adeel Khilji
 */
class ResearchGroup 
{
    private String name;//Declaring instance variable
    private List<Patient> patients;//Declaring instance variable
    
    /**
     * Constructor with two parameters
     * @param name String
     * @param patients List<Patient>
     */
    protected ResearchGroup(String name, List<Patient> patients)
    {
        this.name = name;
        this.patients = patients;
    }
    
    /**
     * getName - getter method
     * @return String
     */
    public String getName()
    {
        return this.name;
    }
    
    /**
     * getListOfPatients - getter method
     * @return List<Patient>
     */
    public List<Patient> getListOfPatients()
    {
        return this.patients;
    }

    /**
     * Display - displays patients in the research group
     */
    public void Display()
    {
        System.out.println("RESEARCH GROUP: " + getName());
        for(Patient p: patients)
        {
            System.out.println("NAME: " + p.getName());
        }
    }
}
