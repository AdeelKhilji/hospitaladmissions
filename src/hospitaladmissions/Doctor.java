/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospitaladmissions;

/**
 * Class Doctor extends Person
 * @author Adeel Khilji
 */
public class Doctor extends Person
{
    private String specialty;//Declaring instance variables
    /**
     * Constructor with two parameters
     * @param name String
     * @param specialty String
     */
    protected Doctor(String name, String specialty)
    {
        super(name);
        this.specialty = specialty;
    }
    /**
     * getSpecialty - getter method
     * @return String
     */
    public String getSpecialty()
    {
        return this.specialty;
    }
    /**
     * toString - overridden method
     * @return 
     */
    @Override
    public String toString()
    {
        return "DOCTOR " + super.toString() + " SPECIALTY: " + getSpecialty();
    }
}
