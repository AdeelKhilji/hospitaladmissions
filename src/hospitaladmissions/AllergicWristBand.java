/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospitaladmissions;

import java.util.List;

/**
 * Class AllergicWristBand extends  WristBand
 * @author Adeel Khilji
 */
public class AllergicWristBand extends WristBand
{
    private List<AllergyMedication> allergicTo;//Declaring instance variables
    
    /**
     * Constructor with three parameters
     * @param barCode BarCode
     * @param medicalDetails MedicalDetails
     * @param allergicTo List<AllergyMedication>
     */
    protected AllergicWristBand(BarCode barCode, MedicalDetails medicalDetails, List<AllergyMedication> allergicTo)
    {
        super(barCode, medicalDetails);
        this.allergicTo = allergicTo;
    }
    
    /**
     * getAllergicTo - getter method
     * @return List<AllergyMedication>
     */
    public List<AllergyMedication> getAllergicTo()
    {
        return this.allergicTo;
    }
    
    /**
     * toString - overridden method
     * @return String
     */
    @Override
    public String toString()
    {
        return "ALLERGIC TO: " + getAllergicTo();
    }
}
