/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospitaladmissions;

import java.util.ArrayList;
import java.time.*;
import java.util.List;

/**
 * Class HospitalAdministrationSimulation
 * @author Adeel Khilji
 */
public class HospitalAdministrationSimulation 
{
    public static void main(String[] args)
    {
        //Declaring variables
        AdmissionStaff admin;
        LocalDate date;
        
        Doctor doctor;
        Parent parent;
        Patient patient;
        Medication medication;
        AllergyMedication allergicTo;
        ResearchGroup researchGroup;
        
        BarCode barCode;
        MedicalDetails medicalDetails;
        
        WristBand wristBand;
        AllergicWristBand allergicWristBand;
        WristBandForChild wristBandForChild;
        
        //Creating Array lists for new patient
        List<Parent> parents = new ArrayList<Parent>();//Creating arraylist for parents
        List<Medication> currentMedicationOn = new ArrayList<Medication>();//Creating arraylist for currentMedicationOn
        List<AllergyMedication> medsAllergicTo = new ArrayList<AllergyMedication>();//Creating arraylist for medsAllergicTo
        List<WristBand> wristBands = new ArrayList<WristBand>();//Creating arraylist for wristBands
        List<AllergicWristBand> allergicWristBands = new ArrayList<AllergicWristBand>();//Creating arraylist for allergicWristBands
        List<Doctor> doctors = new ArrayList<Doctor>();//Creating arraylist for doctors
        List<Patient> patients = new ArrayList<Patient>();//Creating arraylist for patients
        List<ResearchGroup> researchGroups = new ArrayList<ResearchGroup>();//Creating arraylist for researchGroups
        List<WristBand> adminWB = new ArrayList<WristBand>();//Creating arraylist for adminWB
        List<ResearchGroup> adminRG = new ArrayList<ResearchGroup>();//Creating arraylist for adminRG
        
        admin = new AdmissionStaff(adminWB, adminRG);//Creating AdmissionStaff object
        date = LocalDate.of(1999, Month.APRIL, 21);//Creating new date object
        doctors.add(new Doctor("Dr.Seema Sherma","Gynaecologist"));//Creating new doctor object and adding it to doctors arraylist
        currentMedicationOn.add(new Medication("Tylanol"));//Creating new medication object and adding it to currentMedicationOn arraylist
        barCode = new BarCode("1000000a");//Creating new barcode
        medicalDetails = new MedicalDetails(date, doctors, currentMedicationOn);//creating new medical detail object
        wristBand = new WristBand(barCode,medicalDetails);//creating new wristBand object
        researchGroup = new ResearchGroup("Wait Time", patients);//creating new researchGroup object
        researchGroups.add(researchGroup);//adding researchGroup object in to researchGroup arraylist
        wristBands.add(wristBand);//adding wristBand object in to wristBands arraylist
        adminRG.add(researchGroup);//adding researchGroup to adminRG arraylist
        adminWB.add(wristBand);//adding wristBand to adminWG arraylist
        wristBandForChild = new WristBandForChild(barCode, medicalDetails, parents);//creating new wristBandForChild object
        patient = new Patient("Jane Doe", wristBands, allergicWristBands, wristBandForChild, researchGroup);//creating new patient object
        patients.add(patient);//adding patient object in to patient, for researchgroup
        
        //displaying wristbands
        for(WristBand w: wristBands)
        {
            System.out.println("PATIENT NAME: " + patient.getName() + "\tBARCODE: " + w.getBarCode() + "\nMEDICAL DETAILS" + w.getMedicalDetails() + "\nRESEARCH GROUP: " + researchGroup.getName());
        }
        System.out.println();
        
        //new patient
        parents = new ArrayList<Parent>();//creating new arraylist for parents
        currentMedicationOn = new ArrayList<Medication>();//creating new arraylist for currentMedicationOn
        medsAllergicTo = new ArrayList<AllergyMedication>();//creating new arraylist for medsAllergicTo
        wristBands = new ArrayList<WristBand>();//creating new arraylist for wristBands
        allergicWristBands = new ArrayList<AllergicWristBand>();//creating new arraylist for allergicWristBands
        doctors = new ArrayList<Doctor>();//creating new arraylist for doctors
        
        date = LocalDate.of(2001, Month.AUGUST, 12);//creating new date
        doctors.add(new Doctor("Dr.Naila Furqan","Gynaecologist"));//creating new doctor object and adding it in to doctors array
        doctors.add(new Doctor("Dr.Sameena Jawaid","Orthopedic"));//creating new doctors object and adding it in to doctors array
        currentMedicationOn.add(new Medication("Gulocasamine"));//creating new medication object and adding it in to currentMedicationOn
        currentMedicationOn.add(new Medication("Tylanol"));//creating new medication object and adding it in to currentMedicationOn
        currentMedicationOn.add(new Medication("Atenalol"));//creating new medication object and adding it in to currentMedicationOn
        medsAllergicTo.add(new AllergyMedication("penicillin"));//creating new allergyMedication object and adding it in to medsAllergicTo
        medsAllergicTo.add(new AllergyMedication("Anesthesia"));//creating new allergyMedication object and adding it in to medsAllergicTo
        barCode = new BarCode("100a200a");//creating new barCode object
        medicalDetails = new MedicalDetails(date, doctors, currentMedicationOn);//creating new medicalDetails object
        wristBand = new WristBand(barCode,medicalDetails);//creating new wristBand object
        allergicWristBands.add(new AllergicWristBand(barCode, medicalDetails, medsAllergicTo));//creating new allergicWristBand object and adding it in to allergicWristBands arraylist
        researchGroups.add(researchGroup);//adding researchGroup in to researchGroups arraylist
        adminWB.add(wristBand);//adding wristBand in to adminWB arraylist
        patient = new Patient("Thomas Hind", wristBands, allergicWristBands, wristBandForChild,researchGroup);//creating new Patient
        patients.add(patient);//adding new patient in to patients arraylist
        //displaying allergicWristband - displays wristband content and allergicWristband contents
        for(AllergicWristBand aw: allergicWristBands)
        {
            System.out.println("PATIENT NAME: " + patient.getName() + "\tBARCODE: " + aw.getBarCode() + "\nMEDICAL DETAILS" + aw.getMedicalDetails() + "\nALLERGIC TO: " + aw.getAllergicTo() + "\nRESEARCH GROUP: " + researchGroup.getName());
        }
        System.out.println();
        
        //New patient
        parents = new ArrayList<Parent>();//creating new arraylist for parents
        currentMedicationOn = new ArrayList<Medication>();//creating new arraylist for currentMedicationOn
        medsAllergicTo = new ArrayList<AllergyMedication>();//creating new arraylist for medsAllergicTo
        wristBands = new ArrayList<WristBand>();//creating new arraylist for wristBands
        allergicWristBands = new ArrayList<AllergicWristBand>();//creating new arraylist for allergicWristBands
        doctors = new ArrayList<Doctor>();//creating new arraylist for doctors
        
        date = LocalDate.of(2015, Month.MAY, 19);//creating new date
        doctors.add(new Doctor("Dr.Thomas Morron","Pediatrician"));//creating new doctor object and adding it to the doctors arraylist
        doctors.add(new Doctor("Dr.Saima Baig","Optamologist"));//creating new doctor object and adding it to the doctors arraylist
        currentMedicationOn.add(new Medication("Visine"));//creating new medication and adding it to the currentMedicationOn arraylist
        currentMedicationOn.add(new Medication("Vitamine C"));//creating new medication and adding it to the currentMedicationOn arraylist
        currentMedicationOn.add(new Medication("Reneteden"));//creating new medication and adding it to the currentMedicationOn arraylist
        medsAllergicTo.add(new AllergyMedication("IVbrufin"));//creating new allergyMedication object and adding it in to medsAllergicTo arraylist
        medsAllergicTo.add(new AllergyMedication("Steroids"));//creating new allergyMedication object and adding it in to medsAllergicTo arraylist
        parents.add(new Parent("Diego Stwert","Father"));//Creating a new parent object and adding it to the parents arraylist
        parents.add(new Parent("Mary Diego","Mother"));//Creating a new parent object and adding it to the parents arraylist
        barCode = new BarCode("100a340a");//creating a new barcode
        medicalDetails = new MedicalDetails(date, doctors, currentMedicationOn);//creating new medical details
        wristBand = new WristBand(barCode,medicalDetails);//creating new wristband
        allergicWristBands.add(new AllergicWristBand(barCode, medicalDetails, medsAllergicTo));//creating new allergicWristBand and adding it into the allergicWRistBands arraylist
        wristBandForChild = new WristBandForChild(barCode, medicalDetails, parents);//creating new wristBandForChild object
        patient = new Patient("Michelle Stwert", wristBands, allergicWristBands, wristBandForChild, researchGroup);//creating new patient

        //displaying results with wristBandForChild
        System.out.println("PATIENT NAME: "+ patient.getName() +"\nBARCODE: "+ wristBandForChild.toString());
        System.out.println();
        //Displaying researchGroup
        System.out.println("RESEARCH GROUP: ");
        researchGroup.Display();
        System.out.println();
        //Displaying admissionStaff
        System.out.println("ADMISSION STAFF: ");
        admin.Display();
        
    }
}
