/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospitaladmissions;

import java.util.List;

/**
 * Class WristBand
 * @author Adeel Khilji
 */
public class WristBand 
{
    private BarCode barCode;//Declaring instance variable
    private MedicalDetails medicalDetails;//Declaring instance variable
    
    /**
     * Constructor with two parameters
     * @param barCode BarCode
     * @param medicalDetails MedicalDetails
     */
    protected WristBand(BarCode barCode, MedicalDetails medicalDetails)
    {
        this.barCode = barCode;
        this.medicalDetails = medicalDetails;
    }
    
    /**
     * getBarCode - getter method
     * @return BarCode
     */
    public BarCode getBarCode()
    {
        return this.barCode;
    }
    
    /**
     * getMedicalDetails - getter method
     * @return MedicalDetails
     */
    public MedicalDetails getMedicalDetails()
    {
        return this.medicalDetails;
    }
    
    /**
     * toString - overridden method
     * @return String
     */
    @Override
    public String toString()
    {
        return "BARCODE: " + getBarCode() + "\nMEDICAL DETAILS" + getMedicalDetails();
    }
}
