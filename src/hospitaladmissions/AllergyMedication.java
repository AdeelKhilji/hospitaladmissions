/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospitaladmissions;

/**
 * Class AllergyMedication extends Medication
 * @author Adeel Khilji
 */
public class AllergyMedication extends Medication
{
    /**
     * Constructor with one parameter
     * @param name String
     */
    protected AllergyMedication(String name)
    {
        super(name);
    }
    /**
     * toString - overridden method
     * @return String
     */
    @Override
    public String toString()
    {
        return super.getName();
    }
}
