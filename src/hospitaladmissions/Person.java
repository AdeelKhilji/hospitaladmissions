/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospitaladmissions;

/**
 * Class Person
 * @author Adeel Khilji
 */
public class Person 
{
    private String name;//Declaring instance variable
    
    /**
     * Constructor with one parameter
     * @param name String
     */
    protected Person(String name)
    {
        this.name = name;
    }
    
    /**
     * getName - getter method
     * @return String
     */
    public String getName()
    {
        return this.name;
    }
    
    /**
     * toString - overridden method
     * @return String
     */
    @Override
    public String toString()
    {
        return "NAME: " + getName();
    }
}
