/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospitaladmissions;

import java.util.List;

/**
 * Class AdmissionStaff
 * @author Adeel Khilji
 */
public class AdmissionStaff 
{
    private List<WristBand> wristBands;//Declaring instance variables
    private List<ResearchGroup> researchGroups;//Declaring instance variables
    
    /**
     * Constructor with two parameters
     * @param wristBands WirstBand
     * @param researchGroups ResearchGroup
     */
    protected AdmissionStaff(List<WristBand> wristBands, List<ResearchGroup> researchGroups)
    {
        this.wristBands = wristBands;
        this.researchGroups = researchGroups;
    }
    /**
     * getWristBands - getter method
     * @return List<WristBand>
     */
    public List<WristBand> getWristBands()
    {
        return this.wristBands;
    }
    /**
     * getResearchGroups - getter method
     * @return List<ResearchGroup>
     */
    public List<ResearchGroup> getResearchGroups()
    {
        return this.researchGroups;
    }

    /**
     * Display - display method type void
     * Displays list of ResearchGroups and WristBands
     */
    public void Display()
    {
        for(ResearchGroup r: researchGroups)
        {
            System.out.println("RESEARCH GROUPS: " + r.getName());
        }
        for(WristBand wb: wristBands)
        {
            System.out.println("BAR CODE: " + wb.getBarCode());
        }
    }
}
