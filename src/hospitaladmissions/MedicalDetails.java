/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospitaladmissions;

import java.time.LocalDate;
import java.util.List;

/**
 * Class MedicalDetails
 * @author Adeel Khilji
 */
public class MedicalDetails 
{
    private LocalDate dateOfBirth;//Declaring instance variables
    private List<Doctor> doctors;//Declaring instance variables
    private List<Medication> medications;//Declaring instance variables
    
    /**
     * Constructor with three parameters
     * @param dateOfBirth LocalDate
     * @param doctors List<Doctor>
     * @param medications List<Medication>
     */
    protected MedicalDetails(LocalDate dateOfBirth, List<Doctor> doctors, List<Medication> medications)
    {
        this.dateOfBirth = dateOfBirth;
        this.doctors = doctors;
        this.medications = medications;
    }
    
    /**
     * getDate - getter method
     * @return LocalDate
     */
    public LocalDate getDate()
    {
        return this.dateOfBirth;
    }
    
    /**
     * getDoctor - getter method
     * @return List<Doctor>
     */
    public List<Doctor> getDoctors()
    {
        return this.doctors;
    }
    
    /**
     * getMedications - getter method
     * @return List<Medication>
     */
    public List<Medication> getMedications()
    {
        return this.medications;
    }
    
    /**
     * toString - overridden method
     * @return String
     */
    @Override
    public String toString()
    {
        return "\nDATE OF BIRTH: "+ getDate() + "\nTREATED BY: " + getDoctors() + "\nMEDICATIONS: " + getMedications();
    }
}
